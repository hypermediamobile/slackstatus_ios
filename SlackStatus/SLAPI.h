//
//  SLAPI.h
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 07.03.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PLIST(A) [[NSBundle mainBundle] objectForInfoDictionaryKey:A]

#define MESSAGE(M) [((ViewController *)[[[UIApplication sharedApplication] keyWindow] rootViewController]) message:M]

#define void_block_param(a)    (void(^)(void))a
#define success_block_param(a) (void (^)(NSDictionary *))a

@interface SLAPI : NSObject

/*!
 * @brief Joins channel status. If it doesn't exist, creates one, then joins
 */
+ (void)joinChannelWithSuccess:success_block_param(success);

/*!
 * @brief sends message to channel status
 * @param message message to send
 */
+ (void)sendMessage:(NSString *)message;

/*!
 * @brief Reads timestamp for message in channel
 * @discussion Timestamp is saved in user defaults
 * @return Timestamp or empty string if not timestamp
 */
+ (NSString *)timestamp;

+ (void)saveTimestamp:(NSString *)timestamp;

+ (BOOL)isIncognito;
+ (void)setIncognito:(BOOL)incognito;

/*!
 * @brief Reads beacon configuration from url
 * @return Array of dictionaries with beacon info
 */
+ (void)getBeaconsInfo:success_block_param(success);

@end
