//
//  ViewController.h
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 07.03.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(void)message:(NSString *)message;

@end

