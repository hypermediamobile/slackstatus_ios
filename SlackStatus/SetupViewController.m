//
//  SetupViewController.m
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 12.04.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@end

@implementation SetupViewController

- (void) viewDidAppear:(BOOL)animated
{
    NSString *s = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    if(s)
        self.nameTextField.text = s;
    else
        self.nameTextField.text = @"";
}


- (IBAction)didTapOK:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)message:(NSString *)message
{
    //TEMP: empty implementation
}

@end
