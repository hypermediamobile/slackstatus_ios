//
//  ViewController.m
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 07.03.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import "ViewController.h"
#import "SLAPI.h"
#import "SetupViewController.h"

#define S(A) [NSString stringWithFormat:A,username]

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *spyButton;

@end

@implementation ViewController
NSString *username;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textField.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SLAPI joinChannelWithSuccess:^(NSDictionary *dictionary) {
        username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
        if(!username){
            username = @"";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SetupVC"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
}

-(void)message:(NSString *)message
{
    self.textView.text = [NSString stringWithFormat:@"%@\n%@",self.textView.text, message];
}

#pragma mark - user interaction

- (IBAction)didTapRemote:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ works from home :house:")];
}

- (IBAction)didTapSick:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ is on sick leave :cold_sweat:")];
}

- (IBAction)didTapLate:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ will be late :hourglass_flowing_sand:")];
}

- (IBAction)didTapOffline:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ is offline :sleeping:")];
}

- (IBAction)didTapDoNotDisturb:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ is very busy :sweat:")];
}

- (IBAction)didTapNormal:(id)sender
{
    [SLAPI sendMessage:S(@"@%@ is back")];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [SLAPI sendMessage:[NSString stringWithFormat:@"@%@: %@",username,textField.text]];
    textField.text = @"";
    return YES;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.textField resignFirstResponder];
}

- (IBAction)didTapSpy:(id)sender
{
    BOOL incognito = [SLAPI isIncognito];
    self.spyButton.alpha = incognito?0.1:1.0;
    [SLAPI setIncognito:!incognito];
}

@end
