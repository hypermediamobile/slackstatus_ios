//
//  SLAPI.m
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 07.03.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import "SLAPI.h"
#import "AFNetworking.h"
#import "ViewController.h"

@implementation SLAPI

+ (void)callMethod:(NSString *)method
    withParameters:(NSDictionary *)parameters
           success:success_block_param(success)
           failure:void_block_param(failure)
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    NSString *url = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ConfigURL"];
    NSString *token = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"SlackToken"]
                       stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    url = [NSString stringWithFormat:@"https://slack.com/api/%@", method];
    NSMutableDictionary *paramsWithToken = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [paramsWithToken addEntriesFromDictionary:@{@"token":token}];
    
    [manager POST:url parameters:paramsWithToken
         progress:^(NSProgress * _Nonnull downloadProgress) {
             
         }
         success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
             if(success)
                 success(responseObject);
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failure();
         }
     ];
}

+ (void)simpleGet:(NSString *)url
          success:success_block_param(success)
          failure:void_block_param(failure)
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager GET:url parameters:@""
         progress:^(NSProgress * _Nonnull downloadProgress) {
             
         }
         success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
              if(success)
                  success(responseObject);
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failure();
         }
     ];
}


+ (void)joinChannelWithSuccess:success_block_param(success)
{
    [SLAPI callMethod:@"channels.join" withParameters:@{@"name":@"#status"} success:success failure:nil];
}

+ (void)sendMessage:(NSString *)message
{
    NSString *timestamp = [SLAPI timestamp];
    if(!timestamp || [timestamp isEqualToString:@""]) {

        [SLAPI callMethod:@"chat.postMessage"
          withParameters:@{
                    @"text"       : message,
                    @"channel"    : @"status",// #status
                    @"username"   : @"SlackStatus bot",
                    @"icon_emoji" : @":runner:",
                    @"parse"      : @"full",
                    @"link_names" : @1
                    }
              success:^(NSDictionary *d) {
                  NSString *timestamp = d[@"ts"];
                  if(timestamp) {
                      [SLAPI saveTimestamp:timestamp];
                  }
              }
              failure:nil];
        
    } else {
        //update message as we have last timestamp
        [SLAPI callMethod:@"chat.update"
           withParameters:@{
                            @"text"       : message,
                            @"ts"         : timestamp,
                            @"channel"    : @"status",//C0QSX4D45
                            @"as_user"    : @YES,
                            @"parse"      : @"full",
                            @"link_names" : @1
                            }
                  success:^(NSDictionary *d) {
                      NSString *timestamp = d[@"ts"];
                      if(timestamp) {
                          [SLAPI saveTimestamp:timestamp];
                      } else {
                          [SLAPI saveTimestamp:@""];
                          [SLAPI callMethod:@"chat.postMessage"
                             withParameters:@{
                                              @"text"       : message,
                                              @"channel"    : @"status",
                                              @"username"   : @"SlackStatus bot",
                                              @"icon_emoji" : @":runner:",
                                              @"parse"      : @"full",
                                              @"link_names" : @1
                                              }
                                    success:^(NSDictionary *d) {
                                        NSString *timestamp = d[@"ts"];
                                        if(timestamp) {
                                            [SLAPI saveTimestamp:timestamp];
                                        }
                                    }
                                    failure:nil];

                      }
                  }
                  failure:^{
                      //on failure we need to post message as if it was never posted before
                      //TODO: fix this lousy logic
                      [SLAPI saveTimestamp:@""];
                      [SLAPI callMethod:@"chat.postMessage"
                         withParameters:@{
                                          @"text"       : message,
                                          @"channel"    : @"status",
                                          @"username"   : @"SlackStatus bot",
                                          @"icon_emoji" : @":runner:",
                                          @"parse"      : @"full",
                                          @"link_names" : @1
                                          }
                                success:^(NSDictionary *d) {
                                    NSString *timestamp = d[@"ts"];
                                    if(timestamp) {
                                        [SLAPI saveTimestamp:timestamp];
                                    }
                                }
                                failure:nil];
                  }];
    }
    
}

+ (NSString *)timestamp
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"slackstatus_timestamp"];
}

+ (void)saveTimestamp:(NSString *)timestamp
{
    [[NSUserDefaults standardUserDefaults] setObject:timestamp forKey:@"slackstatus_timestamp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isIncognito
{
    return [SLAPI readProperty:@"incognito"];
}


+ (void)setIncognito:(BOOL)incognito
{
    [SLAPI saveProperty:@"incognito" value:incognito];
}

+ (void)saveProperty:(NSString *)key value:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:value] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)readProperty:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

+ (void)saveIntegerProperty:(NSString *)key value:(NSInteger)value
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:value] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)readIntegerProperty:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
}

+ (void)getBeaconsInfo:success_block_param(success)
{
    NSString *url = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ConfigURL"];
    [SLAPI simpleGet:url
             success:^(NSDictionary *response) {
                 success(response);
             }
             failure:^{}
    ];
}

@end
