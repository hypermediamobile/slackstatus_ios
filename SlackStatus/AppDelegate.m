//
//  AppDelegate.m
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 07.03.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import "AppDelegate.h"
#import <EstimoteSDK/EstimoteSDK.h>
#import "SLAPI.h"
#import "ViewController.h"

@interface AppDelegate () <ESTBeaconManagerDelegate>
@property (nonatomic) ESTBeaconManager *beaconManager;
@property (strong,nonnull) NSString *username;
@property (strong,nonnull) NSDictionary *beaconConfig;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.beaconManager = [ESTBeaconManager new];
    self.beaconManager.delegate = self;
    [SLAPI joinChannelWithSuccess:^(NSDictionary *dictionary) {
        [SLAPI getBeaconsInfo:^(NSDictionary *response) {
            self.beaconConfig = response;
            [self startMonitoring];
        }];
        
        self.username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    }];
    return YES;
}

#define D(A) [d valueForKey:A]

- (void)startMonitoring
{
    [self.beaconManager requestAlwaysAuthorization];
 
    // TODO: not checking if there are <=20 beacons
    for(NSDictionary *d in self.beaconConfig) {
        NSString *beaconID = D(@"beacon_id");
        NSString *locationName = D(@"location_name");
        NSString *beaconMajor = D(@"beacon_major");
        NSString *beaconMinor = D(@"beacon_minor");
        CLBeaconMajorValue major = [beaconMajor longLongValue];
        CLBeaconMajorValue minor = [beaconMinor longLongValue];
        [self.beaconManager startMonitoringForRegion:[[CLBeaconRegion alloc]
                                                      initWithProximityUUID:[[NSUUID alloc]
                                                                             initWithUUIDString:beaconID]
                                                      major:major
                                                      minor:minor
                                                      identifier:locationName]
        ];
    }
}

- (void)beaconManager:(id)manager didEnterRegion:(CLBeaconRegion *)region
{
    if(![SLAPI isIncognito])
        [SLAPI sendMessage:[NSString stringWithFormat:@"@%@ has entered: biurko",self.username]];
}

- (void)beaconManager:(id)manager didExitRegion:(nonnull CLBeaconRegion *)region
{
    if(![SLAPI isIncognito])
        [SLAPI sendMessage:[NSString stringWithFormat:@"@%@ has left: biurko",self.username]];
}

- (void)beaconManager:(id)manager monitoringDidFailForRegion:(CLBeaconRegion *)region withError:(NSError *)error
{
//    MESSAGE(@"Error: monitoring hasn't started.");
}

- (void)beaconManager:(id)manager didStartMonitoringForRegion:(CLBeaconRegion *)region
{
//    MESSAGE(@"Monitoring started");
}

@end
