//
//  SetupViewController.h
//  SlackStatus
//
//  Created by Pawel Bragoszewski on 12.04.2016.
//  Copyright © 2016 hypermedia.pl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetupViewController : UIViewController

-(void)message:(NSString *)message;

@end
